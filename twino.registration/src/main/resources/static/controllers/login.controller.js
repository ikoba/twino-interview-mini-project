﻿(function() {
	'use strict';

	angular.module('app').controller('LoginController', LoginController);

	LoginController.$inject = [ '$location', '$rootScope', 'UserService',
			'FlashService' ];
	function LoginController($location, $rootScope, UserService, FlashService) {
		var vm = this;

		vm.login = login;

		function login() {
			vm.dataLoading = true;
			UserService.GetByName(vm.name).then(function(res) {
				if (res.success) {
					$rootScope.globals.currentUser = res.data;
					$location.path('/');
				} else {
					FlashService.Error("Wrong User name");
					vm.dataLoading = false;
				}
			});

		}
		;
	}

})();
