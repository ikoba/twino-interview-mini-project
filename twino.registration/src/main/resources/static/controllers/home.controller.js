﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['UserService','FlashService', '$rootScope'];
    function HomeController(UserService,FlashService, $rootScope) {
        var vm = this;
        vm.update = update;
        vm.user = initUser($rootScope.globals.currentUser);
        
        vm.allUsers = [];
        
        function update(){
        	var user={};
        	user.id = vm.user.id;
        	user.phoneNumber = vm.user.phoneNumber;
        	user.name = vm.user.name;
        	user.salary= vm.user.salary;
        	user.liabilities= vm.user.liabilities;
        	user.birthDate = new Date(vm.user.birthDate);
        	UserService.Update(user).then(function(response){
        		console.log(response);
        		if (response.success) {
                    FlashService.Success(response.data, true);
                    vm.user = initUser(vm.user);
                    vm.dataLoading = false;
                } else {
                    FlashService.Error(response.data);
                    vm.dataLoading = false;
                }
        	});
        }
       function initUser(user){
    	   var toRet = {};
    	   toRet = user;
    	   var age = FlashService.calculateAge(new Date(user.birthDate));
           toRet.birthDate = FlashService.formatDate(new Date(user.birthDate));
           toRet.creditLimit = ((age < 21 || age>75) ? 0 : age*100+user.salary-user.liabilities);
           return toRet;

       }
    }

})();