﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('UserService', UserService);

    UserService.$inject = ['$http'];
    function UserService($http) {
        var service = {};

        service.GetAll = GetAll;
        service.GetByName = GetByName;
        service.Create = Create;
        service.Update = Update;

        return service;

        function GetAll() {
            return $http.get('http://localhost:8090/getAll').then(handleSuccess, handleError('Error getting all users'));
        }

        function GetByName(name) {
            return $http.get('http://localhost:8090/user/'+name).then(handleSuccess, handleError('Error getting user by username'));
        }

        function Create(user) {
            return $http.post('http://localhost:8090/user/', user).then(handleSuccess, handleError('Error creating user'));
        }

        function Update(user) {
            return $http.put('http://localhost:8090/user/', user).then(handleSuccess, handleError('Error updating user'));
        }

        // private functions

        function handleSuccess(res) {
        	console.log(res);
            return { success: true, data: res.data };
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();
