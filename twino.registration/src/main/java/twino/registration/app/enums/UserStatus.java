package twino.registration.app.enums;

public enum UserStatus {
	PUBLIC, REGISTERED, PRIVATE
}
