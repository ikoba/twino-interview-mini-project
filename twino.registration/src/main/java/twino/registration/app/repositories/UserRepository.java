package twino.registration.app.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import twino.registration.app.common.User;

@Transactional
public interface UserRepository extends CrudRepository<User, Long> {
	
	public User findUserByName(String name);

}
