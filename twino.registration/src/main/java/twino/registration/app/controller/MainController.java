package twino.registration.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import twino.registration.app.common.User;
import twino.registration.app.repositories.UserRepository;

@Controller
public class MainController {

	@RequestMapping(value = "/", method=RequestMethod.GET)
	public String get() {
		return "index.html";
	}
	
	@RequestMapping(value = "/user", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<User> create(@RequestBody User jsonData) {
		try {
			userDao.save(jsonData);
			
		} catch (Exception ex) {
			return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<User>(jsonData,HttpStatus.OK);
	}

	@RequestMapping(value = "/user/{name}", method=RequestMethod.GET) 
	@ResponseBody 
	public ResponseEntity<User> findUserByName(@PathVariable("name") String name){
		User u = userDao.findUserByName(name);
		if(u == null) return new ResponseEntity<User>(u,HttpStatus.NOT_FOUND);
		return new ResponseEntity<User>(u,HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/user", method=RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<String> updateUser(@RequestBody User user) {
		try {
			userDao.save(user);
		} catch (Exception ex) {
			return new ResponseEntity<String>("Error while updating user",HttpStatus.INTERNAL_SERVER_ERROR);

		}
		return new ResponseEntity<String>("Successfully updated user",HttpStatus.OK);
	}
	
	@RequestMapping("/getAll")
	@ResponseBody
	public List<User> getAllUsers() {
		return (List<User>) userDao.findAll();
	}
	

	@Autowired
	private UserRepository userDao;

}