## Overview

Create a web application in which user can register and view and edit his profile.

### Registration fields:

* Name
* Phone number
* Birthdate
* Monthly salary
* Current remaining liabilities
* Avatar (will be considered as a bonus)

When user registers they must be redirected to the profile view with the information they specified during registration + allowed credit limit which should be calculated using formula :

    ageInYears * 100 + salary - liabilities

For clients with age < `21` or > `75` credit limit should be `0`

Clients with age < `21` should not be able to register at all.

Clients also must be able to edit their profile.

## Technical requirements

Backend Platform: Spring Boot

Frontend Platform: HTML5, Bootstrap, AngularJS

RDBMS: any embedded SQL database via JPA

Project Management: Gradle (bonus) or Maven

Solution should be submitted as a link to a fork of this repository.

The solution should be submitted before the deadline stated in the email and it is okay to send it incomplete, just make it runnable and implement as many requirements as you can within the allocated time.

Additional requirements:

* All fields must be validated
* Birthdate should only be editable once
* Tests: use JUnit or Spock (bonus)
* Application must be runnable via build system (mvn or gradle) command without any additional installations besides git, java and the build system itself.

## Bonus

Home page must show list of registered users. Users should be able to set visibility of their avatars to `public`, `registered` or `private`.

* if it's `public` it is shown in the list always
* if it's `registered` it is only shown to logged in users
* if it's `private` it is not shown to anyone except the owner themselves.