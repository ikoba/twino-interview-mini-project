﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('RegisterController', RegisterController);

    RegisterController.$inject = ['UserService', '$location', '$rootScope', 'FlashService'];
    function RegisterController(UserService, $location, $rootScope, FlashService) {
        var vm = this;

        vm.register = register;

        function register() {
            vm.dataLoading = true;
            if(FlashService.calculateAge(new Date(vm.user.birthDate))<21){
            	 FlashService.Error("Too young to register");
                 vm.dataLoading = false;
                 return;
            }
            UserService.GetByName(vm.user.name).then(function(response){
            	if(response.success){
            		FlashService.Error("User with this name already exists");
                    vm.dataLoading = false;
                    return;
            	}
                UserService.Create(vm.user)
                .then(function (response) {
                    if (response.success) {
                        FlashService.Success('Registration successful', true);
                        $rootScope.globals.currentUser= response.data;
                        $location.path('/');
                    } else {
                        FlashService.Error(response.message);
                        vm.dataLoading = false;
                    }
                });
            });
        
        }
        
    }

})();
